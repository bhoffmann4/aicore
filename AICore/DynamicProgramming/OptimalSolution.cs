﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AICore.MarkovDecissionProcess;

namespace AICore.DynamicProgramming {
    public class OptimalSolution {
        internal OptimalSolution(IDictionary<uint, double> values, IDictionary<uint, MDP_Action> policy) {
            this.values = new ReadOnlyDictionary<uint, double>(new Dictionary<uint, double>(values));

            var labeledPolicy = new Dictionary<uint, string>();

            foreach(var policyPair in policy) labeledPolicy.Add(policyPair.Key, policyPair.Value.description);

            this.policy = new ReadOnlyDictionary<uint, string>(labeledPolicy);
            }


        public ReadOnlyDictionary<uint, double> values { get; private set; }

        public ReadOnlyDictionary<uint, string> policy { get; private set; }
        }
    }
