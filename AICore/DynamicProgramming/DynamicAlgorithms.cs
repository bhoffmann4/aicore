﻿using System;
using System.Collections.Generic;
using System.Linq;
using AICore.MarkovDecissionProcess;

namespace AICore.DynamicProgramming {
    public static class DynamicAlgorithms {
        private static double _PRECISSION = 0.0000001;

        private static void evaluatePolicy(MDP_System mdpSystem, IDictionary<uint, double> values, IDictionary<uint, MDP_Action> policy, double discountRate) {
            var delta = 0.0;

            do {
                foreach(var state in mdpSystem.states) {
                    var newValue = 0.0;

                    foreach(var edge in policy[state.id].edges) newValue += edge.probability * (edge.reward + discountRate * values[edge.targetState.id]);

                    delta = Math.Max(delta, Math.Abs(values[state.id] - newValue));

                    values[state.id] = newValue;
                    }
                }
            while(Math.Abs(delta) >= _PRECISSION);
            }

        private static bool isPolicyImproved(MDP_System mdpSystem, IDictionary<uint, double> values, IDictionary<uint, MDP_Action> policy, double discountRate) {
            var isImproved = false;

            foreach(var state in mdpSystem.states) {
                MDP_Action bestAction = policy[state.id];
                var bestActionsValue = double.NegativeInfinity;

                foreach(var action in state.actions) {
                    var newValue = 0.0;

                    foreach(var edge in action.edges) newValue += edge.probability * (edge.reward + discountRate * values[edge.targetState.id]);

                    if(newValue > bestActionsValue) {
                        bestActionsValue = newValue;
                        bestAction = action;
                        }
                    }

                if(policy[state.id].description != bestAction.description) {
                    isImproved = true;
                    policy[state.id] = bestAction;
                    }
                }

            return isImproved;
            }

        public static OptimalSolution policyIteration(MDP_System mdpSystem, double discountRate) {
            if(mdpSystem is null) throw new ArgumentNullException(nameof(mdpSystem));
            if(discountRate < 0 || discountRate > 1) throw new ArgumentException("CLASS: DynamicAlgorithms, METHOD: policyIteration - invalid discount rate!");

            var values = new Dictionary<uint, double>();
            var policy = new Dictionary<uint, MDP_Action>();

            foreach(var state in mdpSystem.states) {
                values.Add(state.id, 0);

                if(state.isTerminal()) policy.Add(state.id, new MDP_Action("Terminal state action", 0, state, new List<MDP_Edge>()));
                else policy.Add(state.id, state.actions.First());
                }

            do {
                evaluatePolicy(mdpSystem, values, policy, discountRate);
                }
            while(isPolicyImproved(mdpSystem, values, policy, discountRate));

            var returnPolicy = new Dictionary<uint, string>();

            foreach(var policyPair in policy) returnPolicy.Add(policyPair.Key, policyPair.Value.description);

            return new OptimalSolution(values, returnPolicy);
            }
        }
    }
