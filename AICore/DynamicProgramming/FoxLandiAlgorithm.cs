﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AICore.MarkovDecissionProcess;

namespace AICore.DynamicProgramming {
    public class ChainDecompoisition {
        internal ChainDecompoisition(IEnumerable<MDP_State> recurrenceClasses, IEnumerable<MDP_State> transientStates) {
            this.recurrenceClasses = new List<uint>(recurrenceClasses.Select(recurrenceClass => recurrenceClass.id)).AsReadOnly();
            this.transientStates = new List<uint>(transientStates.Select(state => state.id)).AsReadOnly();
            }

        public ReadOnlyCollection<uint> recurrenceClasses { get; private set; }
        public ReadOnlyCollection<uint> transientStates { get; private set; }
        }
    

    public static class FoxLandiAlgorithm {
        public static ChainDecompoisition decompositeMarkovChain(MDP_System mdpSystem, IDictionary<uint, uint> policy) {
            if(mdpSystem is null) throw new ArgumentNullException(nameof(mdpSystem));
            if(policy is null) throw new ArgumentNullException(nameof(policy));
            if(!mdpSystem.isPolicyValid(policy)) throw new ArgumentException("CLASS: FoxLandiAlgorithm, METHOD: decompositeMakovChain - ivnalid policy!");

            throw new NotImplementedException();
            }
        }
    }
