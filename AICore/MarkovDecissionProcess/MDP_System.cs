﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Security;
using System.Xml;

namespace AICore.MarkovDecissionProcess {
    public class MDP_System {
        private static double _PRECISSION = 0.0000001;

        private readonly List<MDP_State> _states;

        private MDP_System(XmlDocument mdpSystemData) {
            _states = new List<MDP_State>();
            states = _states.AsReadOnly();

            var statesAsText = mdpSystemData.GetElementsByTagName("StateIds")[0].InnerText.Split(',');

            foreach(var stateAsText in statesAsText) _states.Add(new MDP_State(Convert.ToUInt32(stateAsText)));

            var actionNodes = mdpSystemData.GetElementsByTagName("Action");

            foreach(XmlNode actionNode in actionNodes) {
                var sourceStateId = Convert.ToUInt32(actionNode.Attributes["SourceStateId"].InnerText);
                var sourceState = _states.Find(state => state.id == sourceStateId);

                if(sourceState is null) throw new Exception("CLASS: MDP_System, CONSTRUCTOR - unavailable source state!");

                var actionsId = Convert.ToUInt32(actionNode.Attributes["Id"].InnerText);
                var actionDescription = actionNode.Attributes["Description"].InnerText;
                var actionsAverageReward = 0.0;
                var probabilityChecksum = 0.0;
                var actionsEdges = new List<MDP_Edge>();

                var edgeNodes = actionNode.ChildNodes;

                foreach(XmlNode edgeNode in edgeNodes) {
                    var targetStateId = Convert.ToUInt32(actionNode.Attributes["TargetStateId"].InnerText);
                    var targetState = _states.Find(state => state.id == targetStateId);

                    if(targetState is null) throw new Exception("CLASS: MDP_System, CONSTRUCTOR - unavailable target state!");

                    targetState.addParentalState(sourceState);

                    var edgeProbability = Convert.ToDouble(edgeNode.Attributes["Probability"].InnerText);
                    var edgeReward = Convert.ToDouble(edgeNode.Attributes["Reward"].InnerText);

                    if(edgeProbability < 0 || edgeProbability > 1) throw new Exception("CLASS: MDP_System, CONSTRUCTOR - invalid probability!");

                    probabilityChecksum += edgeProbability;

                    actionsEdges.Add(new MDP_Edge(edgeProbability, edgeReward, sourceState, targetState));
                    actionsAverageReward += edgeProbability * edgeReward;
                    }

                if(Math.Abs(probabilityChecksum - 1.0) > _PRECISSION) throw new Exception("CLASS: MDP_System, CONSTRUCTOR - invalid distribution!");

                sourceState.addAction(new MDP_Action(actionsId, actionDescription, actionsAverageReward, sourceState, actionsEdges));
                }
            }

        internal ReadOnlyCollection<MDP_State> states { get; private set; }


        /// <summary>
        /// Creates an instance of the MDP_System class.
        /// </summary>
        /// <exception cref="XmlException">Is thrown, if there is a load or parse error in the XML.</exception>
        /// <exception cref="ArgumentException">Is thrown, if <paramref name="filename"/> is a zero-length string, contains only white space, or 
        /// contains one or more invalid characters as defined by InvalidPathChars.</exception>
        /// <exception cref="ArgumentNullException">Is thrown, if <paramref name="filename"/> is null.</exception>
        /// <exception cref="PathTooLongException">Is thrown, if the specified path, file name, or both exceed the system-defined maximum length.</exception>
        /// <exception cref="DirectoryNotFoundException">Is thrown, if the specified path is invalid.</exception>
        /// <exception cref="IOException">Is thrown, if an I/O error occurred while opening the file.</exception>
        /// <exception cref="UnauthorizedAccessException">Is thrown, if this operation is not supported on the current platform, the caller does not have the required 
        /// permission, <paramref name="filename"/> specifies a file that is read-only or is a directory.</exception>
        /// <exception cref="FileNotFoundException">Is thrown, if the file specified in <paramref name="filename"/> was not found.</exception>
        /// <exception cref="NotSupportedException">Is thrown, if <paramref name="filename"/> is in an invalid format.</exception>
        /// <exception cref="SecurityException">Is thrown, if the caller does not have the required permission.</exception>
        public static MDP_System createInstance(string filename) {
            if(filename is null) throw new ArgumentNullException(nameof(filename));

            var mdpSystemData = new XmlDocument();
            mdpSystemData.Load(filename);

            return new MDP_System(mdpSystemData);
            }

        public bool isPolicyValid(IDictionary<uint, uint> policy) {
            if(policy.Count != _states.Count) return false;

            foreach(var state in _states) {
                if(!policy.ContainsKey(state.id) || !state.actions.Select(action => action.id).Contains(policy[state.id])) return false;
                }

            return true;
            }
        }
    }
