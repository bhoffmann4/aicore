﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AICore.MarkovDecissionProcess {
    internal class MDP_Action {
        public MDP_Action(uint id, string description, double averageReward, MDP_State sourceState, IEnumerable<MDP_Edge> edges) {
            this.id = id;
            this.averageReward = averageReward;
            this.description = description;
            this.sourceState = sourceState;
            this.edges = new ReadOnlyCollection<MDP_Edge>(new List<MDP_Edge>(edges));
            }

        public uint id { get; private set; }

        public double averageReward { get; private set; }

        public string description { get; private set; }

        public MDP_State sourceState { get; private set; }

        public ReadOnlyCollection<MDP_Edge> edges { get; private set; }
        }
    }
