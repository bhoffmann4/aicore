﻿namespace AICore.MarkovDecissionProcess {
    internal class MDP_Edge {
        public MDP_Edge(double probability, double reward, MDP_State sourceState, MDP_State targetState) {
            this.probability = probability;
            this.reward = reward;

            this.sourceState = sourceState;
            this.targetState = targetState;
            }
           
        public double probability { get; private set; }
        public double reward { get; private set; }

        public MDP_State sourceState { get; private set; }
        public MDP_State targetState { get; private set; }
        }
    }
