﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AICore.MarkovDecissionProcess {
    internal class MDP_State {
        private readonly List<MDP_State> _parentalStates;

        private readonly List<MDP_Action> _actions;


        internal void addAction(MDP_Action action) {
            _actions.Add(action);
            }

        internal void addParentalState(MDP_State state) {
            if(!_parentalStates.Contains(state)) _parentalStates.Add(state);
            }


        public MDP_State(uint id) {
            _parentalStates = new List<MDP_State>();
            parentalStates = new ReadOnlyCollection<MDP_State>(_parentalStates);

            _actions = new List<MDP_Action>();
            actions = new ReadOnlyCollection<MDP_Action>(_actions);

            this.id = id;
            }

        public uint id { get; private set; }

        public ReadOnlyCollection<MDP_State> parentalStates { get; private set; }

        public ReadOnlyCollection<MDP_Action> actions { get; private set; }

        public bool isTerminal() {
            return actions.Count == 0;
            }
        }
    }
