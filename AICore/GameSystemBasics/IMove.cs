﻿using System;

namespace AICore.GameSystemBasics {
    public interface IMove : IEquatable<IMove> {
        Player nextPlayer { get; }
        Player player { get; }
        }
    }
