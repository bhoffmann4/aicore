﻿namespace AICore.GameSystemBasics {
    public class Player {
        public Player(byte id) {
            this.id = id;
            }

        public byte id { get; private set; }

        public override int GetHashCode() {
            return base.GetHashCode();
            }

        public override bool Equals(object obj) {
            return obj is Player player && player.id == id;
            }
        }
    }
