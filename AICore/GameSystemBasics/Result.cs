﻿using System;

namespace AICore.GameSystemBasics {
    public struct Result {
        public static Player noWinner = new Player(byte.MaxValue);

        public Result(Player winner, double relativeVictoryPoints) {
            if(relativeVictoryPoints < 0 || relativeVictoryPoints > 1) 
                throw new ArgumentException("STRUCT: Result, CONSTRUCTOR - invalid given relative victory points!");

            this.winner = winner;
            this.relativeVictoryPoints = relativeVictoryPoints;
            }

        public double relativeVictoryPoints { get; private set; }

        public Player winner { get; private set; }
        }
    }
