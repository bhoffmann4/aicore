﻿using System.Collections.ObjectModel;

namespace AICore.GameSystemBasics {
    public interface IChanceMove : IMove {
        ReadOnlyCollection<double> childProbabilities();
        }
    }
