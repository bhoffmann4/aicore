﻿namespace AICore.MonteCarloTreeSearch.GameTreeBasics {
    public interface IFinalChildSelectionService {
        IGameTreeNode selectChildNode(IGameTreeNode node);
        }
    }
