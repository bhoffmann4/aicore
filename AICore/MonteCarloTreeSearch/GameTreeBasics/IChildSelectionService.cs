﻿namespace AICore.MonteCarloTreeSearch.GameTreeBasics {
    public interface IChildSelectionService {
        IGameTreeNode selectChildNode(IGameTreeNode node, double explorationConstant);
        }
    }
