﻿using System.Collections.ObjectModel;

namespace AICore.MonteCarloTreeSearch.GameTreeBasics {
    public interface IGameTreeNode {
        long playouts { get; }
        long playoutsAMAF { get; }

        double value { get; }
        double valueAMAF { get; }

        bool areChildNodesExpanded { get; }

        IGameTreeNode parentNode { get; }

        ReadOnlyCollection<IGameTreeNode> childNodes();
        }
    }
