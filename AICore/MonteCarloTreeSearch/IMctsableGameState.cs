﻿using AICore.GameSystemBasics;
using System.Collections.ObjectModel;

namespace AICore.MonteCarloTreeSearch {
    public interface IMctsableGameState {
        Player player { get; }

        void makeMove(IMove move);

        bool isGameOver();

        IMctsableGameState duplicate();

        ReadOnlyCollection<IMove> possibleMoves();

        Result gameResult();
        }
    }
