﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using AICore.GameSystemBasics;

namespace AICore.MonteCarloTreeSearch {
    internal sealed class MctsBackpropagation {
        private List<IMove> _pathToResult;


        public MctsBackpropagation(Result result, IEnumerable<IMove> pathToResult) {
            _pathToResult = new List<IMove>(pathToResult);
            this.pathToResult = new ReadOnlyCollection<IMove>(_pathToResult);
            this.result = result;
            }

        public Result result { get; private set; }

        public ReadOnlyCollection<IMove> pathToResult { get; private set; }

        public void addMoveToPath(IMove move) {
            _pathToResult.Insert(0, move);
            }
        }
    }
