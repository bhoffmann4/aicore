﻿using System;
using System.Collections.Generic;
using AICore.GameSystemBasics;
using AICore.MonteCarloTreeSearch.GameTreeBasics;
using System.Threading;
using System.Threading.Tasks;

namespace AICore.MonteCarloTreeSearch {
    public class MctsClient {
        private static readonly double _DEFAULT_EXPLORATION_CONSTANT = 4;

        private readonly IChildSelectionService _childSelectionService;

        private readonly IFinalChildSelectionService _finalChildSelectionService;

        private MctsNode selectChild(MctsNode node) {
            var selectedChild = _childSelectionService.selectChildNode(node, _DEFAULT_EXPLORATION_CONSTANT) as MctsNode;

            if(selectedChild is null) 
                throw new InvalidOperationException("CLASS: MctsClient, METHOD: selectChild - child selection service returns no MctsNode!");

            return selectedChild;
            }

        private MctsNode selectFinalChild(MctsNode node) {
            var selectedChild = _finalChildSelectionService.selectChildNode(node) as MctsNode;

            if(selectedChild is null) 
                throw new InvalidOperationException("CLASS: MctsClient, METHOD: selectFinalChild - final child selection service returns no MctsNode!");

            return selectedChild;
            }

        private MctsBackpropagation mctsSearch(IMctsableGameState gameState, MctsNode node) {
            if(!node.areChildNodesExpanded) {
                if(node.playouts == 0) return defaultPolicy(gameState);

                node.expandNode();
                }

            var bestNode = selectChild(node);

            gameState.makeMove(bestNode.move);

            MctsBackpropagation backpropagation;

            if(!gameState.isGameOver()) backpropagation = mctsSearch(gameState, bestNode);
            else backpropagation = new MctsBackpropagation(gameState.gameResult(), new List<IMove>());

            bestNode.addResult(backpropagation.result);
            bestNode.addResultToAMAFChilds(backpropagation);

            backpropagation.addMoveToPath(bestNode.move);

            return backpropagation;
            }

        private MctsBackpropagation defaultPolicy(IMctsableGameState gameState) {
            var gameStateForSimulation = gameState.duplicate();
            var rng = new Random();
            var pathToResult = new List<IMove>();

            while(!gameState.isGameOver()) {
                var possibleMoves = gameStateForSimulation.possibleMoves();
                var chosenMove = possibleMoves[rng.Next(possibleMoves.Count)];

                gameStateForSimulation.makeMove(chosenMove);
                pathToResult.Add(chosenMove);
                }

            return new MctsBackpropagation(gameStateForSimulation.gameResult(), pathToResult);
            }


        public MctsClient(IChildSelectionService childSelectionService, IFinalChildSelectionService finalChildSelectionService) {
            if(childSelectionService is null) throw new ArgumentNullException(nameof(childSelectionService));
            if(finalChildSelectionService is null) throw new ArgumentNullException(nameof(finalChildSelectionService));

            this._childSelectionService = childSelectionService;
            this._finalChildSelectionService = finalChildSelectionService;
            }

        public long iterations { get; private set; }

        public IMove optimalMove(IMctsableGameState initialGameState, long iterations) {
            if(initialGameState is null) throw new ArgumentNullException(nameof(initialGameState));
            if(initialGameState.isGameOver())
                throw new ArgumentException("CLASS: MctsClient, METHOD: optimalMove - the given game state is a terminal one!");

            var root = new MctsRootNode(initialGameState);

            root.expandNode();

            MctsBackpropagation backpropagation;

            for(long i = 1; i <= iterations; i++) {
                backpropagation = mctsSearch(initialGameState.duplicate(), root);

                root.addResult(backpropagation.result);
                root.addResultToAMAFChilds(backpropagation);
                }

            return selectFinalChild(root).move;
            }

        public IMove optimalMove(IMctsableGameState initialGameState, int duration) {
            if(initialGameState is null) throw new ArgumentNullException(nameof(initialGameState));
            if(initialGameState.isGameOver())
                throw new ArgumentException("CLASS: MctsClient, METHOD: optimalMove - the given game state is a terminal one!");
            if(duration < 0) throw new ArgumentException("CLASS: MctsClient, METHOD: optimalMove - the given duration is negative!");

            var root = new MctsRootNode(initialGameState);

            root.expandNode();

            MctsBackpropagation backpropagation;

            var tokenSource = new CancellationTokenSource();

            var workingTask = Task.Factory.StartNew(() => {
                while(!tokenSource.Token.IsCancellationRequested) {
                    backpropagation = mctsSearch(initialGameState.duplicate(), root);

                    iterations++;

                    root.addResult(backpropagation.result);
                    root.addResultToAMAFChilds(backpropagation);
                    }
                }, tokenSource.Token);

            tokenSource.CancelAfter(duration);

            workingTask.Wait();

            tokenSource.Dispose();

            return selectFinalChild(root).move;
            }
        }
    }
