﻿using System;
using AICore.GameSystemBasics;

namespace AICore.MonteCarloTreeSearch {
    internal sealed class MctsRootNode : MctsNode {
        public MctsRootNode(IMctsableGameState initialGameState) : base(initialGameState.player) {
            if(initialGameState is null) throw new ArgumentNullException(nameof(initialGameState));

            this.initialGameState = initialGameState;
            }

        public IMctsableGameState initialGameState { get; private set; }

        public new void addResult(Result result) {
            playouts++;
            }
        }
    }
