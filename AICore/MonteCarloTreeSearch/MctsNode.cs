﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using AICore.GameSystemBasics;
using AICore.MonteCarloTreeSearch.GameTreeBasics;

namespace AICore.MonteCarloTreeSearch {
    internal class MctsNode : IGameTreeNode {
        private List<IGameTreeNode> _childNodes;

        private void addResultAMAF(Result result) {
            playoutsAMAF++;

            if(result.winner.id == move.player.id) valueAMAF += 1 + result.relativeVictoryPoints;
            }


        internal MctsNode(Player player) {
            _childNodes = new List<IGameTreeNode>();

            this.player = player;
            }

        public static IMctsableGameState calculateGameStateFromNode(MctsNode node) {
            var pathInGameTree = new List<IGameTreeNode>();
            var currentNode = node;

            pathInGameTree.Add(node);

            while(!(currentNode.parentNode is null)) {
                pathInGameTree.Insert(0, currentNode.parentNode);
                currentNode = currentNode.parentNode as MctsNode;
                }

            if(!(currentNode is MctsRootNode root)) 
                throw new InvalidOperationException("CLASS: MctsNode, METHOD: calculateGameStateFromNode - no root node available in the tree");

            var gameState = root.initialGameState.duplicate();

            pathInGameTree.RemoveAt(0);

            foreach(MctsNode pathNode in pathInGameTree) gameState.makeMove(pathNode.move);

            return gameState;
            }
            
        public MctsNode(MctsNode parentNode, IMove move) {
            _childNodes = new List<IGameTreeNode>();

            this.move = move;
            this.parentNode = parentNode;
            this.player = move.nextPlayer;

            areChildNodesExpanded = false;
            }

        public long playouts { get; protected set; }
        public long playoutsAMAF { get; protected set; }

        public double value { get; protected set; }
        public double valueAMAF { get; protected set; }

        public bool areChildNodesExpanded { get; private set; }

        public IMove move { get; private set; }

        public IGameTreeNode parentNode { get; private set; }

        public Player player { get; private set; }

        public void addResult(Result result) {
            playouts++;

            if(result.winner.id == move.player.id) value += 1 + result.relativeVictoryPoints;
            }

        public void addResultToAMAFChilds(MctsBackpropagation backpropagation) {
            if(backpropagation is null) throw new ArgumentNullException(nameof(backpropagation));

            if(!areChildNodesExpanded) return;

            foreach(MctsNode child in _childNodes) {
                foreach(var pathMove in backpropagation.pathToResult) {
                    if(child.move.Equals(pathMove)) child.addResultAMAF(backpropagation.result);
                    }
                }
            }

        public void expandNode() {
            if(areChildNodesExpanded) return;

            areChildNodesExpanded = true;

            var gameState = MctsNode.calculateGameStateFromNode(this);
            var possibleMoves = gameState.possibleMoves();

            foreach(var possibleMove in possibleMoves) {
                if(possibleMove is IChanceMove chanceMove) _childNodes.Add(new MctsChanceNode(this, chanceMove, chanceMove.childDistribution));
                else _childNodes.Add(new MctsNode(this, possibleMove));
                }
            }

        public ReadOnlyCollection<IGameTreeNode> childNodes() {
            if(!areChildNodesExpanded) expandNode();

            return new ReadOnlyCollection<IGameTreeNode>(_childNodes);
            }
        }
    }
