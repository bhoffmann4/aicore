﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using AICore.GameSystemBasics;

namespace AICore.MonteCarloTreeSearch {
    internal sealed class MctsChanceNode : MctsNode {
        private static readonly double _PRECISSION = 0.0000001;

        private readonly List<double> _childDistribution;


        public MctsChanceNode(MctsNode parentNode, IChanceMove move, IEnumerable<double> childProbabilities) : base(parentNode, move) {
            if(childProbabilities is null) throw new ArgumentNullException(nameof(childProbabilities));

            expandNode();

            if(childProbabilities.Count() != childNodes().Count)
                throw new ArgumentException("CLASS: MctsChanceNode, CONSTRUCTOR - the number of the given probabilities does not coincide witht the number of moves!");
            if(childProbabilities.Any(probability => probability < 0))
                throw new ArgumentException("CLASS: MctsChanceNode, CONSTRUCTOR - the distribution contains at least one negative value!");
            if(Math.Abs(childProbabilities.Sum() - 1.0) > _PRECISSION)
                throw new ArgumentException("CLASS: MctsChanceNode, CONSTRUCTOR - invalid distribution!");

            _childDistribution = new List<double>(childProbabilities);
            this.childProbabilities = new ReadOnlyCollection<double>(_childDistribution);
            }

        public ReadOnlyCollection<double> childProbabilities { get; private set; }

        public void addResult(Result result, MctsNode backpropagatingNode) {
            if(backpropagatingNode is null) throw new ArgumentNullException(nameof(backpropagatingNode));
            if(!childNodes().Contains(backpropagatingNode)) 
                throw new ArgumentException("CLASS: MctsChanceNode, METHOD: addResult - the given node is not a child node!");

            playouts++;

            if(result.winner.id == move.player.id) value += childProbabilities[childNodes().IndexOf(backpropagatingNode)] * (1 + result.relativeVictoryPoints);
            }
        }
    }
